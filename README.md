# apobates.yalla

#### 介绍
JavaFx Json Beautifier

#### 软件架构
项目是一个基于OpenJdk 17.0.6写的JavaFx 17.0.6小工具. 实现Json内容的获取及格式化


#### 环境配置

1.  JAVA_HOME. 版本: 17.0.6

下载地址为: [java-se/17](https://www.oracle.com/java/technologies/downloads/#java17)

Windows 环境配置

![JAVA_HOME](java_home.png)

2.  JAVAFX SDK. 版本: 17.0.6, 版本与JDK保持一致

下载地址为: [javafx 17.0.6](https://gluonhq.com/products/javafx/)

Windows 环境配置

![输入图片说明](2023-03-22%20194616.png)

#### 打包运行

1.  使用Build Artifacts打成jar. 在命令行运行

![输入图片说明](build-4.png)

菜单中: Build > Build Artifacts > Build 或者 ReBuild. jar会写入到项目的out目录中。

切换到apobates.yalla.jar的路径. 复制以下命令:



```
java --module-path "%JAVA_FX%\lib" --add-modules javafx.controls,javafx.base,javafx.fxml,javafx.graphics,javafx.media,javafx.web -Djava.library.path="%JAVA_FX%\bin" -jar apobates.yalla.jar

```


如图所示:

![输入图片说明](build-5.png)


或者将根文件夹中的yalla.bat文件复制到编译后的文件夹中，双击yalla.bat即可打开程序。注意：bat文件中默认的jar名称是: `json-formatter.jar`


2.  使用maven的插件: javafx:jlink,  使用生成的App.bat. 此种方式不依赖JavaFx的运行库

![输入图片说明](build-6.png)

运行后将在target目录中生成app.zip和app目录，两者一个是压缩后的，一个是未压缩的。切换到app\bin\app.bat运行即可

![输入图片说明](build-7.png)

3.  可以独主运行的Jar, 主要有两点: A. 新建的主类(App), 在main文件中调用MainApplication.main方法. B. 打包时使用App作为入口类,将JAVAFX SDK的bin作为文件引入. 如下图:

![输入图片说明](build-830.png)

![输入图片说明](build-831.png)

4.  项目目前界面如下:

![输入图片说明](BEA1D835-0AEE-4830-8193-C6445883456F.png)

左侧的文本框是待格式的字符串, 右侧的文本框是格式后的字符串. 左侧也可以输入返回json的URL此时格式化按钮会变为：请求Json，点击后响应内容格式化后显示在右侧的文本框

#### 最后说明

1.  项目有使用未模块的依赖. 在maven package/install时会提示：
> Required filename-based automodules detected: [minimal-json-0.9.5.jar, eventbus-java-3.3.1.jar]. Please don't publish this project to a public artifact repository! 

MANIFEST.MF 中使用：Automatic-Module-Name

```
Manifest-Version: 1.0
Main-Class: apobates.gui.formatter.MainApplication
Class-Path: javafx-graphics-17.0.6.jar javafx-base-17.0.6.jar javafx-con
 trols-17.0.6.jar javafx-controls-17.0.6-win.jar javafx-graphics-17.0.6-
 win.jar javafx-fxml-17.0.6.jar gson-2.10.1.jar javafx-fxml-17.0.6-win.j
 ar javafx-base-17.0.6-win.jar
Automatic-Module-Name: com.eclipsesource.json org.greenrobot.eventbus
```

2.  第一次写GUI可能出现小问题请客官多提意见



