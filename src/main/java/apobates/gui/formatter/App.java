package apobates.gui.formatter;


/**
 * 打包成可独主运行jar时使用它作为主类
 * @since 20230227
 */
public class App {
    public static void main(String[] args) {
        MainApplication.main(args);
    }
}
