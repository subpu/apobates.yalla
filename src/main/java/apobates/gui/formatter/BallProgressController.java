package apobates.gui.formatter;

import apobates.gui.formatter.component.autoComplete.jfoenix.JFXAutoCompletePopup;
import apobates.gui.formatter.component.progress.ball.BallProgressIndicator;
import apobates.gui.formatter.component.progress.ball.BallProgressIndicatorSkin;
import apobates.gui.formatter.component.progress.crystalBall.CrystalBallProgressIndicator;
import apobates.gui.formatter.component.progress.crystalBall.CrystalBallProgressIndicatorSkin;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import java.util.List;
// 测试控制器
public class BallProgressController {
    // 当前stage
    @FXML
    private VBox rootLayout;
    @FXML
    private Slider ballSlide;
    @FXML
    private BallProgressIndicator ballProgress;
    @FXML
    private CrystalBallProgressIndicator crystalBallProgress;
    @FXML
    private Slider crystalBallSlide;
    @FXML
    private ComboBox<String> autoFillBox;
    @FXML
    private ListView<String> result;
    @FXML
    private TextField recordItem;
    @FXML
    private Button recordAdd;
    //@FXML
    //private YallaAutoComplete<String> completeBox;
    private List<String> frameLog = new ArrayList<>();
    private ObservableList<String> observableList = FXCollections.observableArrayList(frameLog);
    @FXML
    public void initialize(){
        this.ballSlide.setMin(0);
        this.ballSlide.setMax(100);
        this.ballSlide.setValue(0);
        //
        this.ballProgress.setSkin(new BallProgressIndicatorSkin(this.ballProgress));
        //
        // slider.valueProperty().addListener((o, oldVal, newVal) -> indicator.setProgress(newVal.intValue()));
        //
        this.ballSlide.valueProperty().addListener((ObservableValue<? extends Number> observableValue, Number number, Number newVal)-> {
            int curStep = (newVal.intValue());
            System.out.println("[CP]++++"+curStep);
            ballProgress.setProgress(curStep);
        });
        // ---------------------------------------------------->
        // completeBox.addAll(List.of("HELLO", "TROLL", "WFEWEF", "WEF"));
        // completeBox.setPredicate((item) -> item.toLowerCase().contains(completeBox.getTextInput().getText().toLowerCase()));
        // ---------------------------------------------------->

        autoFillBox.setEditable(true);
        autoFillBox.getItems().addAll("HELLO", "TROLL", "WFEWEF", "WEF");
        JFXAutoCompletePopup<String> autoCompletePopup = new JFXAutoCompletePopup<>();
        autoCompletePopup.getSuggestions().addAll(autoFillBox.getItems());
        //autoCompletePopup.hide();
        // SelectionHandler sets the value of the comboBox
        autoCompletePopup.setSelectionHandler(event -> {
            autoFillBox.setValue(event.getObject());
        });

        final TextField editor = autoFillBox.getEditor();
        editor.textProperty().addListener((ObservableValue<? extends String> observableValue, String s, String t1)-> {
            System.out.println("[FS]empty:" + autoCompletePopup.getFilteredSuggestions().isEmpty());
            System.out.println("[FS]show:" + autoFillBox.showingProperty().get());
            System.out.println("[FS]vlau:" + t1);
            // The filter method uses the Predicate to filter the Suggestions defined above
            // I choose to use the contains method while ignoring cases
            autoCompletePopup.filter(item -> item.toLowerCase().contains(editor.getText().toLowerCase()));
            // Hide the autocomplete popup if the filtered suggestions is empty or when the box's
            // original popup is open
            if (autoCompletePopup.getFilteredSuggestions().isEmpty() || autoFillBox.showingProperty().get()) {
                autoCompletePopup.hide();
            } else {
                autoCompletePopup.show(editor);
            }
        });
        // ----------------------------->
        // 输出
        result.setItems(observableList);
        // rmtpConsole.getItems().add("::ffmpeg push start::");
        result.getItems().add("::ffmpeg push start::");
        // ----------------------------->
        this.crystalBallSlide.setMin(0);
        this.crystalBallSlide.setMax(100);
        this.crystalBallSlide.setValue(0);
        //
        this.crystalBallProgress.setSkin(new CrystalBallProgressIndicatorSkin(this.crystalBallProgress));
        //
        this.crystalBallSlide.valueProperty().addListener((ObservableValue<? extends Number> observableValue, Number number, Number newVal)-> {
            int curStep = (newVal.intValue());
            System.out.println("[CPC]++++"+curStep);
            crystalBallProgress.setProgress(curStep);
        });
    }

    @FXML
    protected void addRecordAction(ActionEvent event){
        // result.getItems().add(recordItem.getText());
        observableList.add("::"+recordItem.getText());
    }
}
