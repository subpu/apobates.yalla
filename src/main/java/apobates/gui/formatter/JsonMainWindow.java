package apobates.gui.formatter;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Stage;
import org.fxmisc.richtext.CodeArea;
import java.io.IOException;

/**
 * 主启动界面
 */
public class JsonMainWindow {
    private final Stage stage;

    public JsonMainWindow(Stage stage) {
        this.stage = stage;
    }
    public FXMLLoader open()throws IOException{
        final FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("yalla-view.fxml"));
        // 场景
        Scene scene = new Scene(fxmlLoader.load(), 1127, 758);
        scene.getRoot().requestFocus();
        // 舞台
        stage.setTitle("Yalla Json Beautifier");
        stage.setScene(scene);
        // 最小宽度
        stage.setMinWidth(600);
        stage.setMinHeight(700);

        // 侦听舞台大小变化
        stage.widthProperty().addListener((ObservableValue<? extends Number> observableValue, Number number, Number t1)-> {
            Platform.runLater(() ->{
                Scene scene1 = stage.getScene();
                System.out.println("[JMW]change width value:" + scene1.getWidth() + ", height value:"+scene1.getHeight());
                // codearea
                CodeArea rightEditor = (CodeArea)scene1.lookup("#rightView");
                // 给滚动条留出位置(10)
                rightEditor.setPrefWidth(scene1.getWidth() * 0.50f - 10);
                rightEditor.setPrefHeight(scene1.getHeight() - 80);
                /* ------------------------------*/
                // 使用：GridPane.hgrow控制
                // ALWAYS：布局区域将始终尝试增长（或缩小），共享那些空间;
                // SOMETIMES:如果没有控件设置为ALWAYS，或者其它控件没有处理完变化的控件，设置为SOMETIMES的控件将和其它控件分享这些区域。
                // NEVER:控件不会参与处理变化的空间。
                /* ------------------------------*/
            });
        });
        stage.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue)-> {
            Platform.runLater(() -> {
                Scene scene1 = stage.getScene();
                System.out.println("[JMW]height change value:" + scene1.getHeight());
                // codearea
                CodeArea rightEditor = (CodeArea) scene1.lookup("#rightView");
                rightEditor.setPrefHeight(scene1.getHeight()-80);
            });
        });
        // 侦听窗口的关闭按钮(右上角)
        stage.setOnCloseRequest(e->{
            System.out.println("[MA]window close button");
            e.consume();
            // 由Stop来接管
            JsonController primaryCTL = (JsonController)fxmlLoader.getController();
            primaryCTL.existAction(null);
        });
        // 组合按键的侦听
        // CTRL+O打开外部资源表单
        KeyCodeCombination openResourceKCC = new KeyCodeCombination(KeyCode.O, KeyCodeCombination.CONTROL_DOWN);
        scene.getAccelerators().put(openResourceKCC, () ->{
            System.out.println("触发: 外部资源控制器");
            try {
                JsonController primaryCTL = (JsonController)fxmlLoader.getController();
                primaryCTL.openJsonResourceCTL();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        // CTRL+S保存右侧的格式化内容
        KeyCodeCombination saveResultKCC = new KeyCodeCombination(KeyCode.S, KeyCodeCombination.CONTROL_DOWN);
        scene.getAccelerators().put(saveResultKCC, () ->{
            System.out.println("触发: 保存操作");
            JsonController primaryCTL = (JsonController)fxmlLoader.getController();
            primaryCTL.saveFormatJsonResult();
        });
        // CTRL+E退出系统
        KeyCodeCombination existKCC = new KeyCodeCombination(KeyCode.E, KeyCodeCombination.CONTROL_DOWN);
        scene.getAccelerators().put(existKCC, () ->{
            System.out.println("[MA]hotkey: CTRL+E close");
            stage.close();
        });
        stage.show();
        return fxmlLoader;
    }
}
