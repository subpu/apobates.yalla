package apobates.gui.formatter;

/**
 * Json字符串解析异常
 */
public class JsonParseException extends Exception{
    public JsonParseException(String message) {
        super(message);
    }

    public JsonParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonParseException(Throwable cause) {
        super(cause);
    }
}
