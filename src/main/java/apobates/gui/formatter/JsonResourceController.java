package apobates.gui.formatter;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * JavaFx GUI Json外部资源控制器
 * @since 20230227
 */
public class JsonResourceController {
    // 当前stage
    @FXML
    private VBox resourceLayout;
    // --------------------------------------------->以上均作废
    @FXML
    private TabPane resourceTabPane;
    @FXML
    private RemoteURLJSONController remoteURLJSONController;
    @FXML
    private LocalFileJSONController localFileJSONController;

    @FXML
    public void initialize(){
        // 默认启用
        this.initLocalFileTab();
        // 监听tabpane组件的改变
        resourceTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue)-> {
            System.out.println("Tab active is: " + newValue.getId());
            // 切到: URL
            if(newValue.getId().equals("embedRemoteURLPane")){
                this.initRemoteURLTab();
            }
        });
    }
    private void initLocalFileTab(){
        if(null == localFileJSONController){
            localFileJSONController = new LocalFileJSONController();
        }
    }
    private void initRemoteURLTab(){
        if(null == remoteURLJSONController){
            remoteURLJSONController = new RemoteURLJSONController();
        }
    }

    private Stage getCurrentStage(){
        return (Stage) resourceLayout.getScene().getWindow();
    }
}
