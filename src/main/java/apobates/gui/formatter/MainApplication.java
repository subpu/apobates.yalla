package apobates.gui.formatter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.application.Platform;
import java.io.IOException;

/**
 * JavaFx程序的入口类
 * module javafx.graphics does not "opens javafx.scene.text" to module com.jfoenix
 * @since 20230227
 */
public class MainApplication extends Application {
    private FXMLLoader primaryLoader;
    @Override
    public void start(Stage stage) throws IOException {
        this.primaryLoader = new JsonMainWindow(stage).open();
    }
    // 程序结束.包括文件退出/窗口的关闭按钮
    // 程序非正常结束?
    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("[MA]application: stop");
        JsonController primaryCTL = (JsonController)this.primaryLoader.getController();
        primaryCTL.closeMainController();
        //
        Platform.exit();
    }

    public static void main(String[] args) {
        launch();
    }
}