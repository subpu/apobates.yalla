package apobates.gui.formatter;

import apobates.gui.formatter.packet.ResourceType;
import apobates.gui.formatter.reader.RemoteForm;
import apobates.gui.formatter.service.RecentExecutorService;
import apobates.gui.formatter.service.RemoteFormLoaderService;
import apobates.gui.formatter.storage.RecentRecord;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * 从URL的响应中获取json字符串
 */
public class RemoteURLJSONController {
    @FXML
    private VBox remoteURLTab;
    @FXML
    private TextField resourceURL;
    @FXML
    private TextField urlParamter;
    @FXML
    private ComboBox rourceCharset;
    @FXML
    private Slider requestTime;
    @FXML
    private Button requestURLBtn;
    @FXML
    private Button cancelRequestURLBtn;
    @FXML
    private ListView<String> remoteHistory;
    private RecentExecutorService recentExecutorService;
    // 资源请求表单
    private RemoteForm remoteForm = new RemoteForm();

    @FXML
    public void initialize(){
        // 增加ComboBox
        rourceCharset.getItems().addAll(RecentExecutorService.getSupportCharset());
        // 将输入控件与form绑定::双向绑定
        // 请求URL
        remoteForm.filePathProperty().bindBidirectional(resourceURL.textProperty());
        // 字符集
        rourceCharset.valueProperty().bindBidirectional(remoteForm.fileEncodingProperty());
        remoteForm.setFileEncoding("UTF-8");
        // 超时
        requestTime.valueProperty().bindBidirectional(remoteForm.timeoutProperty());
        remoteForm.setTimeout(108000);
        // 请求类型
        remoteForm.setType(ResourceType.URL);
        // 最近记录执行器@20230402
        recentExecutorService = new RecentExecutorService(remoteForm);
        recentExecutorService.initialize(remoteHistory, ResourceType.URL);
        remoteHistory.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue)-> {
            System.out.println("[RFLS][JRC-URL]show click history::"+newValue);
            // 将选中的值放回到表单中
            Optional<RecentRecord> recentRecord = recentExecutorService.getRecentRecordCodec().decode(newValue);
            if(recentRecord.isPresent()){
                remoteForm.setFilePath(recentRecord.get().getPath());
                remoteForm.setFileEncoding(recentRecord.get().getCharset());
                remoteForm.setTimeout(recentRecord.get().getTimeout());
            }
        });
    }
    @FXML
    protected void requestURLAction(ActionEvent event){
        // 按钮的设置 @20230305-2
        requestURLBtn.setDisable(true);
        requestURLBtn.setText("请求中");
        // 是否存在查询参数@20230804
        if(!urlParamter.textProperty().isEmpty().get()){
            String s ="?".concat(urlParamter.textProperty().get());
            remoteForm.setFilePath(remoteForm.getFilePath().concat(s));
        }
        // 回调
        Consumer<Boolean> fallbackFun = (t1)->{
            System.out.println("[RFLS][JRC-URL]fallback handler status:"+t1);
            if(null != t1 && t1) {
                // 记录请求记录@20230402
                this.recentExecutorService.cache();
                // 关闭
                getCurrentStage().close();
            }
            if(null == t1 || !t1){
                this.requestFail();
            }
        };
        // 资源类型,
        // 若是能通过ToggleGroup能获取到可删除
        String s = "网址";
        System.out.println("[JRC][RRA-URL]current resource type:"+s);
        // 2.执行加载
        RemoteFormLoaderService remoteFormLoaderService = new RemoteFormLoaderService(remoteForm);
        // 3.侦听成功状态(有变化时)/若无变化不会执行
        remoteFormLoaderService.setCallbackFun(fallbackFun);
        remoteFormLoaderService.stream().subscribe((booleanChange)->{
            Boolean t1 = booleanChange.getNewValue();
            System.out.println("[FRLS][JRC-URL]stream subscribe status: "+t1);
        });
        remoteFormLoaderService.load(null);
    }

    @FXML
    protected void cancelRequestURLAction(ActionEvent event){
        getCurrentStage().close();
    }

    private Stage getCurrentStage(){
        return (Stage) remoteURLTab.getScene().getWindow();
    }

    private void requestFail(){
        System.out.println("[JRC-URL]request resource record fail. print resource:::");
        System.out.println(remoteForm);
        // 删除无效的最近记录
        this.recentExecutorService.delete(remoteForm.getType(), remoteForm.getFilePath());
        //
        Alert noConTip = new Alert(Alert.AlertType.ERROR, "网络地址目前暂时无法访问", ButtonType.OK);
        // 置顶
        Stage stage = (Stage) noConTip.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);
        stage.toFront(); // not sure if necessary
        noConTip.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                // 解除禁止
                requestURLBtn.setDisable(false);
                requestURLBtn.setText("开始");
            }
        });
    }
}
