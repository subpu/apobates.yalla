package apobates.gui.formatter.component;

import apobates.gui.formatter.component.mask.MaskPane;
import apobates.gui.formatter.component.mask.MaskPaneSkin;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * 左侧内容加载遮盖层
 */
public class SourceLoaderLayout {
    // 遮盖层
    private MaskPane loadMP;
    private StackPane maskLayout;

    public SourceLoaderLayout(double width, double height){
        // ::遮盖层::
        // 铺盖整个区域
        this.maskLayout = new StackPane();
        maskLayout.setPadding(new Insets(0,0,0,0));
        maskLayout.setPrefWidth(width);
        maskLayout.setPrefHeight(height);
        // maskLayout.setStyle("-fx-background-color: rgba(0, 0, 0 ,.6)");
        this.loadMP = new MaskPane();
        loadMP.setText("玩命填坑中...");
        loadMP.setSkin(new MaskPaneSkin(this.loadMP));
        maskLayout.getChildren().add(loadMP);
    }

    /**
     * 加载遮盖层
     * @param parentNode
     */
    public void show(AnchorPane parentNode){
        parentNode.getChildren().add(maskLayout);
        System.out.println("[SCR]init");
        // ::遮盖层::
        this.loadMP.setVisible(true);
    }

    /**
     * 绑定进度
     * @param property
     */
    public void bind(ReadOnlyDoubleProperty property){
        this.loadMP.progressProperty().bind(property);
    }

    /**
     * 关闭Loading
     * @param parentNode
     */
    public void close(AnchorPane parentNode){
        // ::遮盖层::
        this.loadMP.setVisible(false);
        parentNode.getChildren().remove(this.maskLayout);
    }
}
