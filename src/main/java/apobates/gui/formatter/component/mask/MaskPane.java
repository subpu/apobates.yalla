/**
 * Copyright (c) 2014, 2015, ControlsFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of ControlsFX, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL CONTROLSFX BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package apobates.gui.formatter.component.mask;

import apobates.gui.formatter.component.progress.ball.BallProgressIndicator;
import apobates.gui.formatter.component.progress.crystalBall.CrystalBallProgressIndicator;
import apobates.gui.formatter.component.progress.ring.RingProgressIndicator;
import javafx.beans.property.*;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
/**
 * <p>MaskerPane is designed to be placed alongside other controls in a {@link StackPane},
 * in order to visually mask these controls, preventing them from being accessed
 * for a short period of time. This comes in handy whenever waiting on asynchronous
 * code to finish, and you do not want the user to be able to modify the state
 * of the UI while waiting.</p>
 *
 * <p>To use this control, it is necessary to place it as the last child in a {@link StackPane},
 * with the other children being masked by this MaskerPane when visible. Simply use
 * {@link #setVisible(boolean)} to toggle between visible states.</p>
 */
public class MaskPane extends Control {
    // -- Background Color

    // -- Progress
    private final DoubleProperty progress = new SimpleDoubleProperty(this, "progress", -1.0); //$NON-NLS-1$
    public final DoubleProperty progressProperty() { return progress; }
    public final double getProgress() { return progress.get(); }
    public final void setProgress(double progress) { this.progress.set(progress); }

    // -- Progress Node
    private final ObjectProperty<Node> progressNode = new SimpleObjectProperty<Node>() {
        {
            // 内置的
            // ProgressIndicator node = new ProgressIndicator();
            // node.progressProperty().bind(progress);
            // 环
            // RingProgressIndicator indicator=new RingProgressIndicator();
            /* 遮盖球
            BallProgressIndicator indicator = new BallProgressIndicator();
            indicator.setPrefWidth(160);
            indicator.setPrefHeight(160);
            progressProperty().addListener((o, oldVal, newVal) ->{
                // 转成1-100的进度值
                float curStep = (newVal.floatValue() * 100) + 10;
                indicator.setProgress(Float.valueOf(curStep).intValue());
            });*/
            // 水晶球
            CrystalBallProgressIndicator indicator = new CrystalBallProgressIndicator();
            indicator.setPrefWidth(160);
            indicator.setPrefHeight(160);
            progressProperty().addListener((o, oldVal, newVal) ->{
                // 转成1-100的进度值
                float curStep = (newVal.floatValue() * 100) + 10;
                indicator.setProgress(Float.valueOf(curStep).intValue());
            });
            setValue(indicator);
        }

        @Override public String getName() { return "progressNode"; } //$NON-NLS-1$
        @Override public Object getBean() { return MaskPane.this; }
    };
    public final ObjectProperty<Node> progressNodeProperty() { return progressNode; }
    public final Node getProgressNode() { return progressNode.get();}
    public final void setProgressNode(Node progressNode) {
        this.progressNode.set(progressNode);
    }
    // -- Progress Visibility
    private final BooleanProperty progressVisible = new SimpleBooleanProperty(this, "progressVisible", true); //$NON-NLS-1$
    public final BooleanProperty progressVisibleProperty() { return progressVisible; }
    public final boolean getProgressVisible() { return progressVisible.get(); }
    public final void setProgressVisible(boolean progressVisible) { this.progressVisible.set(progressVisible); }

    // -- Text
    private final StringProperty text = new SimpleStringProperty(this, "text", "Please Wait..."); //$NON-NLS-1$
    public final StringProperty textProperty() { return text; }
    public final String getText() { return text.get(); }
    public final void setText(String text) { this.text.set(text); }
    /**
     * Construct a new {@link MaskPane}
     */
    public MaskPane() {
        this.getStylesheets().add(getClass().getResource("/css/mask-pane.css").toExternalForm());
        getStyleClass().add("masker-pane");
    } //$NON-NLS-1$

    @Override
    protected Skin<?> createDefaultSkin() { return new MaskPaneSkin(this); }
    /*
    private final String getUserAgentStylesheet(Class<?> clazz,
                                                String fileName) {
        if (stylesheet == null) {
            stylesheet = clazz.getResource(fileName).toExternalForm();
        }

        return stylesheet;
    }*/
}
