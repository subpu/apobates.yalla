package apobates.gui.formatter.component.progress.ball;

import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

/**
 * 进度球
 * 使用：矩形遮盖非进度区域
 * @author xiaofanku
 * @since 20230410
 */
public class BallProgressIndicator extends Control {
    // -- Progress
    private ReadOnlyIntegerWrapper progress = new ReadOnlyIntegerWrapper(0);
    private ReadOnlyBooleanWrapper indeterminate = new ReadOnlyBooleanWrapper(false);

    public int getProgress() {
        return progress.get();
    }

    public ReadOnlyIntegerWrapper progressProperty() {
        return progress;
    }

    /**
     * 更新进度值
     * @param progress 进度值(1-100)
     */
    public void setProgress(int progress) {
        this.progress.set(defaultToHundred(progress));
        indeterminate.set(progress < 0);
    }

    public boolean isIndeterminate() {
        return indeterminate.get();
    }

    public ReadOnlyBooleanWrapper indeterminateProperty() {
        return indeterminate;
    }

    public void setIndeterminate(boolean indeterminate) {
        this.indeterminate.set(indeterminate);
    }
    private int defaultToHundred(int value) {
        if (value > 100) {
            return 100;
        }
        return value;
    }
    //
    public BallProgressIndicator(){
        this.getStylesheets().add(getClass().getResource("/css/ball-progress.css").toExternalForm());
        this.getStyleClass().add("ballindicator");
    }

    @Override
    protected Skin<?> createDefaultSkin() { return new BallProgressIndicatorSkin(this); }
}
