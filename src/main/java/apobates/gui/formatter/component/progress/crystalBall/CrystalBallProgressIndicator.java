package apobates.gui.formatter.component.progress.crystalBall;

import apobates.gui.formatter.component.progress.ball.BallProgressIndicator;
import javafx.scene.control.Skin;

/**
 * 进度球
 * 使用：使用ArcTo绘制进度区域
 * @author xiaofanku
 * @since 20230513
 */
public class CrystalBallProgressIndicator extends BallProgressIndicator {
    //
    public CrystalBallProgressIndicator(){
        super();
    }

    @Override
    protected Skin<?> createDefaultSkin() { return new CrystalBallProgressIndicatorSkin(this); }
}
