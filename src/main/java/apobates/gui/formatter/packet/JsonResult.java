package apobates.gui.formatter.packet;

import javafx.beans.property.SimpleStringProperty;
import org.reactfx.Change;
import org.reactfx.EventStream;
import org.reactfx.EventStreams;

/**
 * JsonResourceController的结果
 * 便于与JsonController通信
 * 替代器为: JsonMessageBody
 */
public class JsonResult {
    private static JsonResult instance;
    // 存储待格式化字符串
    private SimpleStringProperty result = new SimpleStringProperty();
    // 存储字符串的来源类型: TEXT/FILE/URL
    private SimpleStringProperty resource = new SimpleStringProperty();
    private JsonResult(){
    }
    static {
        instance = new JsonResult();
    }

    /**
     * 返回Json内容实例
     * @return
     */
    public static JsonResult getInstance(){
        return instance;
    }

    /**
     * 返回Json内容: 待格式化
     * @return
     */
    public String getResult() {
        return result.get();
    }

    public SimpleStringProperty resultProperty() {
        return result;
    }

    /**
     * 设置Json内容: 待格式化
     * @param result
     */
    public void setResult(String result) {
        // 后续变更无法侦听到
        this.result.set(null);
        this.result.set(result);
    }

    /**
     * 若Json内容不可用返回true
     * @return
     */
    public boolean isBlank(){
        return null == this.result.get() || this.result.get().isBlank();
    }

    /**
     * 返回Json内容的来源
     * @return
     */
    public String getResource() {
        return resource.get();
    }

    public SimpleStringProperty resourceProperty() {
        return resource;
    }
    /**
     * 设置Json内容的来源
     * @param resource
     */
    public void setResource(String resource) {
        // 后续变更无法侦听到
        this.resource.set(null);
        this.resource.set(resource);
    }

    public EventStream<Change<String>> stream(){
        return EventStreams.changesOf(result);
    }
}
