package apobates.gui.formatter.packet;

/**
 * Json资源类型枚举
 */
public enum ResourceType {
    URL(1, "网址"),
    FILE(2, "文件"),
    TEXT(3, "文本"),
    JSON(0, "默认");

    private String names;
    private int type;

    ResourceType(int type, String names) {
        this.names = names;
        this.type = type;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static ResourceType getInstance(String typeName){
        ResourceType ins = null;
        for(ResourceType ele : values()){
            if(ele.name().equals(typeName)){
                ins = ele;
                break;
            }
        }
        return ins;
    }
}
