package apobates.gui.formatter.reader;

import apobates.gui.formatter.packet.ResourceType;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * 远程资源获取表单
 */
public class RemoteForm {
    private StringProperty filePath = new SimpleStringProperty();
    private StringProperty fileEncoding = new SimpleStringProperty();
    private SimpleIntegerProperty timeout = new SimpleIntegerProperty();
    private SimpleObjectProperty<ResourceType> type = new SimpleObjectProperty<>(ResourceType.FILE);
    public String getFilePath() {
        return filePath.get();
    }

    public StringProperty filePathProperty() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath.set(filePath);
    }

    public String getFileEncoding() {
        return fileEncoding.get();
    }

    public StringProperty fileEncodingProperty() {
        return fileEncoding;
    }

    public void setFileEncoding(String fileEncoding) {
        this.fileEncoding.set(fileEncoding);
    }

    public int getTimeout() {
        return timeout.get();
    }

    public SimpleIntegerProperty timeoutProperty() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout.set(timeout);
    }

    public ResourceType getType() {
        return type.get();
    }

    public SimpleObjectProperty<ResourceType> typeProperty() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type.set(type);
    }

    public void reset(ResourceType resourceType){
        System.out.println("[RF]reset form value");
        setTimeout(8000);
        setFileEncoding("UTF-8");
        setFilePath(null);
        setType(resourceType);
        System.out.println("[RF]timeout:"+getTimeout()+",path:"+getFilePath()+", encoding:"+getFileEncoding());
    }

    public String toString(){
        return "[RF]timeout:"+getTimeout()+",path:"+getFilePath()+", encoding:"+getFileEncoding();
    }
}
