package apobates.gui.formatter.richedit;

import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Pattern;

/**
 * 单行字符分隔匹配
 */
public final class JsonKeyWordLineMatch extends StylePaneMatch{
    @Override
    public StyleSpans<Collection<String>> computeHighlighting(String text) {
        // System.out.println("HH content:"+text);
        final StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        Pattern pattern = Pattern.compile("\\R");
        pattern.splitAsStream(text).forEach(lineStr->processSingleLine(lineStr, spansBuilder));
        return spansBuilder.create();
    }
    private static void processSingleLine(String lineStr, StyleSpansBuilder<Collection<String>> spansBuilder){
        // System.out.println("HHLine content:"+lineStr);
        // List<SpanStyleClassResult> sscr = new ArrayList<>();
        int seekLength = 0;
        if(lineStr.indexOf(":") != -1){
            // key的结束下标
            int seplitor = lineStr.indexOf(":");
            // key的开始下标
            int firstQuote = lineStr.indexOf("\"");
            // key前置的
            spansBuilder.add(Collections.emptyList(), firstQuote);
            //
            seekLength=firstQuote;
            // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(0, firstQuote),"def", 0, firstQuote, lineStr.length()));
            // Key
            spansBuilder.add(Collections.singleton("key"), seplitor-firstQuote);
            //
            seekLength=seplitor;
            // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(firstQuote, seplitor), "key", firstQuote, seplitor, lineStr.length()));

            //-------------------------------------------------------------------------------
            // Value
            String tmp = lineStr.substring(seplitor+1).trim();
            if(tmp.startsWith("\"") || !tmp.equals("[") || !tmp.equals("{")){
                // 去掉结尾的：逗号
                // value的长度
                int valLength = tmp.endsWith(",") ? tmp.length() -1 : tmp.length();
                // 值的class计算
                String valClass = calcJsonValStyleClass(tmp.substring(0, valLength));
                // 从key的到val之间的长度
                int mid = lineStr.indexOf(tmp);
                spansBuilder.add(Collections.emptyList(), mid-seplitor);
                //
                seekLength=mid;
                // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(seplitor, mid),"def", seplitor, mid, lineStr.length()));
                //
                spansBuilder.add(Collections.singleton(valClass), valLength);
                //
                seekLength=mid+valLength;
                // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(mid, mid+valLength), valClass, mid, mid+valLength, lineStr.length()));
                //
                if(seekLength < lineStr.length()) {
                    spansBuilder.add(Collections.emptyList(), lineStr.length() - seekLength);
                    //
                    seekLength=lineStr.length();
                    // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(mid + valLength), "default", mid + valLength, lineStr.length(), lineStr.length()));

                }
            }else{
                if(lineStr.length() - seplitor > 0) {
                    spansBuilder.add(Collections.emptyList(), lineStr.length() - seplitor);
                    //
                    seekLength=lineStr.length();
                    // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(seplitor),"defClass", seplitor, lineStr.length(), lineStr.length()));

                }
            }
        }
        if(lineStr.length()>seekLength) {
            spansBuilder.add(Collections.emptyList(), lineStr.length() - seekLength);
            // sscr.add(new SpanStyleClassResult(seekLength, lineStr.substring(seekLength),"defStyle", seekLength, lineStr.length(), lineStr.length()));
        }
        // sscr.stream().forEach(System.out::println);
    }

}
