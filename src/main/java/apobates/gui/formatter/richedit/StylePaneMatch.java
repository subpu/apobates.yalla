package apobates.gui.formatter.richedit;

import org.fxmisc.richtext.model.StyleSpans;
import java.util.Collection;

/**
 * 抽像的StylePaneMatch.computeHighlighting
 * 适用于Json子类有:JsonKeyWordLineMatch, JsonKeyWordRegexMatch
 */
public sealed abstract class StylePaneMatch permits JsonKeyWordRegexMatch,JsonKeyWordLineMatch,JavaKeyWordMatch{
    public abstract StyleSpans<Collection<String>> computeHighlighting(String text);

    protected static String calcJsonValStyleClass(String jsonVal){ // 包含双引号
        // System.out.println("[HM]value type calc arg:"+jsonVal);
        String lcStr = jsonVal.toLowerCase();
        if(lcStr.equals("\"true\"") || lcStr.equals("\"false\"")){
            return "bool";
        }
        if(lcStr.equals("null") || lcStr.equals("\"null\"")){
            return "null";
        }
        // 数字
        boolean isNumber = lcStr.replace("\"", "").matches("-?[0-9]+.?[0-9]*");
        if(isNumber){
            return "number";
        }
        if(jsonVal.equals("{") || jsonVal.equals("[")){
            return "default";
        }
        return "string";
    }
}
