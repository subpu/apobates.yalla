package apobates.gui.formatter.richedit.log;

/**
 * 记录richtextfx的分隔结果
 */
public record SpanStyleClassResult (int seekLength, String word, String className, int start, int finish,int total){

    @Override
    public String toString() {
        return "SpanStyleClassResult{" +
                "seek=" + seekLength +
                ", word=" + word +
                ", className='" + className + '\'' +
                ", start=" + start +
                ", finish=" + finish +
                ", total=" + total +
                '}';
    }
}
