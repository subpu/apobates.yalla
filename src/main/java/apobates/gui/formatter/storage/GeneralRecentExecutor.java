package apobates.gui.formatter.storage;

import apobates.gui.formatter.packet.ResourceType;
import java.util.Optional;
import java.util.Set;

/**
 * 最近记录执行器操作接口
 */
public sealed interface GeneralRecentExecutor permits JsonStorageRecentExecutor {
    /**
     * 所有最近请求记录
     * @return 读取失败返回空列表
     */
    Set<RecentRecord> getAll();

    /**
     * 查看指定资源类型最近的所有请求记录
     * @param type Json资源类型
     * @return
     */
    Set<RecentRecord> getType(ResourceType type);

    /**
     * 查看指定资源类型最近的一条请求记录
     * @param type Json资源类型
     * @return
     */
    Optional<RecentRecord> getLastByType(ResourceType type);

    /**
     * 是否存在最近记录
     * @return true存在
     */
    boolean isExists();

    /**
     * 不存在最近记录创建.若已经存在则追加记录
     * @param record 最近资源访问记录
     * @return 返回目前最近记录的数量
     */
    int replace(RecentRecord record);

    /**
     * 删除指定的类型的最近记录
     * @param type Json资源类型
     * @param path 资源路径
     * @return 删除成功返回true
     */
    boolean delete(ResourceType type, String path);
    /**
     * 将内存中的最近记录持久化
     * 若未在内存中临时存储可以使用空实现
     */
    default void persiste(){
        System.out.println("[GRE]No persiste");
    }
}
