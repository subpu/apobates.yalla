package apobates.gui.formatter.storage;

import apobates.gui.formatter.packet.ResourceType;
import apobates.gui.formatter.util.Core;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 最近资源访问记录
 */
public class RecentRecord implements Serializable {
    // 资源路径
    private String path;
    // 资源类型
    private ResourceType type;
    // 字符集
    private String charset;
    // 超时时长
    private int timeout;
    // 记录的时间/unix timestamp
    private int dateTime;
    private final static RecentRecordStringCodec recentRecordStringCodec = new RecentRecordStringCodec();
    // Unable to make field private final java.time.LocalDate java.time.LocalDateTime.date accessible: module java.base does not "opens java.time" to module com.google.gson
    public RecentRecord(String path, ResourceType type) {
        this.path = path;
        this.type = type;
        this.timeout = 8000;
        this.charset = "UTF-8";
        this.dateTime = Core.toTimestamp(LocalDateTime.now());
    }
    public RecentRecord(String path, ResourceType type, String charset, int timeout) {
        this.path = path;
        this.type = type;
        this.timeout = timeout<8000?8000:timeout;
        this.charset = Core.isNotBlank(charset)?charset:"UTF-8";
        this.dateTime = Core.toTimestamp(LocalDateTime.now());
    }
    public RecentRecord() {
    }

    public String getPath() {
        return path;
    }

    public ResourceType getType() {
        return type;
    }

    public int getDateTime() {
        return dateTime;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = Core.toTimestamp(dateTime);
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * 写入到最近记录菜单中的名字
     * @return
     */
    public String toMenuItemName(){
        /* 若是只显示名字呢?造成认知模糊 */
        return recentRecordStringCodec.encode(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecentRecord that)) return false;
        return getPath().equals(that.getPath()) && getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPath(), getType());
    }

    @Override
    public String toString() {
        return "RecentRecord{" +
                "path='" + path + '\'' +
                ", type=" + type +
                ", dateTime=" + dateTime +
                '}';
    }
}
