package apobates.gui.formatter.storage;

import java.util.Optional;

/**
 * 最近结果的编解码器
 * @param <T> 编码后的类型
 */
public interface RecentRecordCodec<T> {
    /**
     * 编码器
     * @param recentRecord
     * @return
     */
    T encode(RecentRecord recentRecord);

    /**
     * 解码器
     * @param value
     * @return
     */
    Optional<RecentRecord> decode(T value);
}
