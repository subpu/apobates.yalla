package apobates.gui.formatter.tree;

import javafx.scene.control.TreeItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public sealed abstract class AbstractJsonNodeBuilder<T> permits JsonNodeTreeGsonBuilder {
    public abstract TreeItem<JsonNodeItem> build(T jsonValue);
    public abstract JsonNodeValueType getValueType(T jsonValue);
    public abstract Map<JsonNodeValueType,Integer> groupValueType(T jsonValue);
    protected static void putIfIncrement(JsonNodeValueType type, Map<JsonNodeValueType,Integer> data){
        if(data.containsKey(type)){
            data.put(type, data.get(type) +1);
        }
        else {
            data.put(type, 1);
        }
    }
    /**
     * 返回所有父节点的names
     * @param node
     * @return
     */
    public static List<String> getParentNames(TreeItem<JsonNodeItem> node){
        List<String> box = new ArrayList<>();
        getParentPath(node, box);
        Collections.reverse(box);
        return box;
    }
    private static void getParentPath(TreeItem<JsonNodeItem> node, List<String> box){
        if(null != node && null != node.getParent()){
            String tmp = node.getParent().getValue().getName();
            box.add(tmp);
            getParentPath(node.getParent(), box);
        }
    }
}
