package apobates.gui.formatter.tree;

/**
 * Json节点类型
 */
public enum JsonNodeValueType {
    DEFULT("Value"),
    BOOLEAN("Boolean"),
    NUMBER("Number"),
    STRING("String"),
    NULL("Null"),
    ROOT("Root"),
    ELEMENT("Element"),
    OBJECT("Object"),
    ARRAY("Array"),
    NODE("Node");

    private String symbol;

    JsonNodeValueType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
