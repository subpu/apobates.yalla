package apobates.gui.formatter.tree;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;

/**
 * 搜索节点表单
 */
public class SearchNodeForm{
    // 搜索节点名字
    private StringProperty name = new SimpleStringProperty();
    // 搜索节点值
    private StringProperty value = new SimpleStringProperty();

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getValue() {
        return value.get();
    }

    public StringProperty valueProperty() {
        return value;
    }

    public void setValue(String value) {
        this.value.set(value);
    }
    // 是否为空.name和value都是空的
    @Override
    public String toString() {
        return "SearchNodeForm{" +
                "name=" + getName() +
                ", value=" + getValue() +
                '}';
    }

    /**
     * 侦听节点名和节点值的连接值
     * @param listener
     */
    public void contact(ChangeListener<? super String> listener){
        valueProperty().concat(nameProperty()).addListener(listener);
    }
}
