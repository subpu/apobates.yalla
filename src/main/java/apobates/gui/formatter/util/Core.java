package apobates.gui.formatter.util;

import apobates.gui.formatter.JsonParseException;
import com.google.gson.*;
import javafx.scene.paint.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

/**
 * 工具类
 */
public final class Core {
    /**
     * 将FX的颜色转成WEB的16进制格式
     * @param color
     * @return
     */
    public static String toRGBCode(Color color) {
        return String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) );
    }

    /**
     * WEB的16进制格式转Color
     * @param rgbCode WEB的16进制格式,例:#FFFFFF
     * @return
     */
    public static Color toColor(String rgbCode){
        return Color.web(rgbCode);
    }
    /**
     * 对参数进行GSON格式化
     * @param str 字符串
     * @return
     */
    public static String prettyJsonString(String str) throws JsonParseException {
        try{
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(parseJson(str));
        }catch (JsonSyntaxException e){
            throw new JsonParseException(e.getCause());
        }
    }
    public static JsonElement parseJson(String str)throws JsonSyntaxException {
        return JsonParser.parseString(str);
    }
    /**
     * 对参数进行GSON格式化
     * @param str 字符串
     * @param indentNumber 缩进字符数/the number of spaces to use
     * @return
     */
    public static String prettyJsonString(String str, int indentNumber) throws JsonParseException {
        try{
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(parseJson(str));
        }catch (JsonSyntaxException e){
            throw new JsonParseException(e.getCause());
        }
    }
    /**
     * 校验参数是否是一个网址
     * @param str
     * @return 是返回URI对象,不是返回NULL
     */
    public static URI buildURI(String str){
        if(!isNotBlank(str) || !str.startsWith("http")){
            return null;
        }
        try {
            return new URI(str).parseServerAuthority();
        } catch (URISyntaxException e) {
            return null;
        }
    }

    /**
     * 目标参数字符串是否可用
     * @param str
     * @return 可用返回true
     */
    public static boolean isNotBlank(String str){
        return str!=null && str.trim().length()>0 && !str.toLowerCase().equals("null");
    }
    /**
     * 返回指定网址的json响应结果
     * @deprecated 
     * @param url 网址
     * @param timeout 超时时间, 单位毫秒
     * @return
     */
    public static String getURLResponseJson(String url, int timeout)  {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(timeout);
            connection.setRequestMethod("GET");
            // 设置请求类型为 application/json
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK){
                return null;
            }
            // 获取输入流
            try(BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
                // 开始读取数据
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                return response.toString();
            }
        }catch (IOException e){

        }
        return null;
    }
    /**
     * 将指定的日期转成unix timestamp
     *
     * @param date 要求不为null,反之返回0
     * @return
     */
    public static int toTimestamp(LocalDateTime date) {
        if (null == date) {
            return 0;
        }
        Long d = date.toEpochSecond(ZoneOffset.UTC);
        return d.intValue();
    }

    /**
     * 使用Optional对目标字符串值的处理
     * null和空字符串时均使用默认值
     * @param target 目标字符串
     * @param defaultVal 默认值
     * @return
     */
    public static String getOptionalVal(String target, String defaultVal){
        return Optional.ofNullable(target).map(ele->ele.isEmpty()?null:ele).orElse(defaultVal);
    }

    /**
     * 使用Optional对目标字符串值的处理
     * @param target 目标字符串
     * @return
     */
    public static Optional<String> getNotEmptyVal(String target){
        return Optional.ofNullable(target).map(ele->ele.isEmpty()?null:ele);
    }

}
