module apobates.gui.jsonformatter {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.base;

    requires com.google.gson;
    requires com.github.albfernandez.juniversalchardet;
    requires okhttp3;
    requires com.google.common;
    requires reactfx;
    requires org.fxmisc.richtext;
    requires org.fxmisc.flowless;
    exports apobates.gui.formatter;
    requires org.kordamp.ikonli.core;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.bootstrapicons;
    // requires galimatias;
    // opens com.sun.javafx.stage to apobates.gui.formatter;
    opens apobates.gui.formatter to javafx.fxml,javafx.graphics;
    exports apobates.gui.formatter.packet;
    opens apobates.gui.formatter.packet to javafx.fxml;

    exports apobates.gui.formatter.tree;
    opens apobates.gui.formatter.tree to javafx.fxml;
    exports apobates.gui.formatter.service;
    opens apobates.gui.formatter.service to javafx.fxml;
    opens apobates.gui.formatter.storage to com.google.gson;
    opens apobates.gui.formatter.component.progress.ball to javafx.fxml;
    opens apobates.gui.formatter.component.progress.crystalBall to javafx.fxml;
}