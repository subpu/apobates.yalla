@echo off
@REM 检测java环境变量
IF NOT EXIST "%JAVA_HOME%\bin\java.exe" goto lostJava
@REM 检测javafx环境变量
if "%JAVA_FX%" == "" goto lostJavaFx
@REM 运行命令
java --module-path "%JAVA_FX%\lib" --add-modules javafx.controls,javafx.base,javafx.fxml,javafx.graphics,javafx.media,javafx.web -Djava.library.path="%JAVA_FX%\bin" -jar json-formatter.jar
exit

:lostJava
echo.
echo Error: JAVA_HOME is set to an invalid directory. >&2
echo JAVA_HOME = "%JAVA_HOME%" >&2
echo Please set the JAVA_HOME variable in your environment to match the >&2
echo location of your Java installation. >&2
echo.
goto error

:lostJavaFx
echo.
echo Error: JAVA_FX is set to an invalid directory. >&2
echo JAVA_FX = "%JAVA_FX%" >&2
echo Please set the JAVA_FX variable in your environment to match the >&2
echo location of your Java installation. >&2
echo.
goto error

:lostJar
echo.
echo Error: build Artifacts default names is:  json-formatter.jar
echo.
goto error

:error
@REM set ERROR_CODE=1
pause & exit
@REM @echo off